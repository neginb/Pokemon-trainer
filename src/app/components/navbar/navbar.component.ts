import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  get user(): User | undefined {
    return this.userService.user;
  }
  

  constructor(
    private readonly userService: UserService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
  }

  //the method for logout button and its navigation
  //put the username to undefinied by logging out in sessionstorage
  onLogoutClick(): void {
    this.router.navigateByUrl("/login");
    this.userService.logoutUser;
    this.userService.user = undefined;
  }

}
