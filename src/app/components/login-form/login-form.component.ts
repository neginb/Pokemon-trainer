import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();

  //DI.
  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService,
  ) { }

  //log in button functionality
  public loginSubmit(loginForm: NgForm): void {

    //username input and inlogging!
    const { username } = loginForm.value;

    //if the trainer has not writen anything on the input box then throw an alert
    if (username === "") {
      alert("Please enter your username!");

    //if the user has writen a username with less than 3 letters, then throw an alert
    } else if (username.length < 3) {

      alert("Your username should be at least 3 letters!")
    } else {

      this.loginService.login(username)
        .subscribe({
          next: (user: User) => {
            this.userService.user = user;
            this.login.emit();
          },
          error: () => {

          }
        })
    }




  }

}
