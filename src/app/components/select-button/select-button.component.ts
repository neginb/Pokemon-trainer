import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { SelectService } from 'src/app/services/select.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-select-button',
  templateUrl: './select-button.component.html',
  styleUrls: ['./select-button.component.css']
})

export class SelectButtonComponent implements OnInit {

  public isSelected: boolean = false;

  @Input() pokemonName: string = "";

  get loading(): boolean {
    return this.selectService.loading;
  }  

  constructor(
    private userService: UserService,
    private readonly selectService: SelectService
  ) { }

  ngOnInit(): void {
    
    //Input are resolved!
    this.isSelected = this.userService.inSelected(this.pokemonName);
  }

  onSelectClick(): void{
    
    //Add the pokemon to the selected list!
    this.selectService.addToSelected(this.pokemonName)
    .subscribe({
      next: (user: User) => {
       this.isSelected = this.userService.inSelected(this.pokemonName);
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message);
      }
    })
    
  }

}
