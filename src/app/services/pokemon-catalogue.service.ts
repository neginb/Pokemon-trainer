import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResults } from '../models/pokemon.model';


const { apiPokemons } = environment;

@Injectable({
  providedIn: 'root'
})

export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;
  

  get pokemons(): Pokemon[]{
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  //finding the pokemons from API
  public findAllPokemones(): void {

    if(this._pokemons.length > 0 || this.loading){
      return;
    } 

    //get the results property from the pokemons API
    this._loading = true;
    this.http.get<PokemonResults>(apiPokemons)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (PokemonResult: PokemonResults) => {
        const pokemons: Pokemon[] = PokemonResult.results;
        this._pokemons = pokemons;

        //a for loop for finding the pokemon id from the api URL to be able to take the pictures of each pokemon from another URL
        for(let i = 0; i < pokemons.length ; i++){
          let array = pokemons[i].url.split("/");
          let id = array[array.length - 2];
          pokemons[i].id = parseInt(id);
        }
        },
        //error handle
        error: (error:HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }

//Get the pokemon with their name for the profile page list
public pokemonByName(name: string): Pokemon | undefined {
  return this._pokemons.find((pokemon: Pokemon) => pokemon.name === name);
}

}
