import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';


@Injectable({
  providedIn: 'root'
})

export class UserService {

  private _user?: User;

  get user(): User | undefined {
    return this._user;
  }

  //call the storageSave method from Utils
  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!); 
    this._user=user;
  }

  //call the storageDelete method from Utils
  set logoutUser(user: User | undefined) {
    StorageUtil.storageDelete; 
    this._user=user;
  }

  //read data from the storage
  constructor() { 
    this._user = StorageUtil.storageRead<User>(StorageKeys.User);
  }

  //when a pokemon is selected this method runs. take the selected pokemons' name to put it in the list
  public inSelected(pokemonName: string): boolean {
    if(this._user) {
      return Boolean(this.user?.pokemon.find((pokemon: Pokemon) => pokemon.name === pokemonName));
    }

    return false;
  }
  
  //add the selected pokemon to the list
  public addToSelected(pokemon: Pokemon): void {
    if(this._user) {
      this._user.pokemon.push(pokemon);
    }
  }

  //by clicking the "check" button the pokemon will be removed from the list
  public removeFromSelected(pokemonName: string): void {
    if(this._user){
      this._user.pokemon = this._user.pokemon.filter((pokemon : Pokemon) => pokemon.name !== pokemonName);
    }
  }
}
