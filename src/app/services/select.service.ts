import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';

//take the API and keys from enviroment file
const { API_KEY, apiUsers } = environment;

@Injectable({
  providedIn: 'root'
})

export class SelectService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly userService: UserService,
  ) { }
  
  //get the pokemon based on the name.
  //Path request with the userId and the pokemon
  public addToSelected(pokemonName: string): Observable<User> {

    if (!this.userService.user) {
      throw new Error("There is no user");
    }

    const user: User = this.userService.user;

    const pokemon : Pokemon | undefined = this.pokemonService.pokemonByName(pokemonName);

    if (!pokemon) {
      throw new Error("No pokemon with name: " + pokemonName);
    }

    //Does pokemon exist in selected list
    if(this.userService.inSelected(pokemonName)) {
      this.userService.removeFromSelected(pokemonName);
    } else {
      this.userService.addToSelected(pokemon);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': API_KEY
    })

    this._loading = true;
    
    //takes the user id from API
    return this.http.patch<User>(`${apiUsers}/${user.id}`, {
      pokemon: [...user.pokemon] //Already updated.
    }, {
      headers
    })
    .pipe(
      tap((updatedUser: User) => {
        this.userService.user = updatedUser;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }


}
