import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';


const { apiUsers, API_KEY } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //Dependency Injection.
  constructor(private readonly http: HttpClient) { }

  //Modules, HttpClient, Observables, and RxJS operators.
  public login(username: string): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | undefined) => {
          if (user === undefined) {
            return this.creatUser(username);
          }
          return of(user);
        })
      )
  }


  //Check if user exists
  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(
        //RxJS Operators
        map((response: User[]) => response.pop())
      )
  }

  //If not user - Create a user
  private creatUser(username: string): Observable<User> {

    //user
    const user = {
      username,
      pokemon: []
    };

    //header -> API key
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": API_KEY
    });
    
    //POST request
    return this.http.post<User>(apiUsers, user, {
      headers
    })

  }



  //If user || Create user -> Store user 
}
