export class StorageUtil {
    //saving the user to the sessionStorage
    public static storageSave<T>(key: string, value: T): void {

        sessionStorage.setItem(key, JSON.stringify(value));
    }

    //delete from the sessionStorage based on the key
    public static storageDelete<T>(key: string): void {

        sessionStorage.removeItem(key);
    }
    
    //read the sessionStorage based on the key
    public static storageRead<T>(key: string): T | undefined  {
        const storedValue = sessionStorage.getItem(key);
        try {

            if (storedValue) {
                return JSON.parse(storedValue) as T;
            }

            return undefined;
            

        }
        catch (e) {
            sessionStorage.removeItem(key);
            return undefined ;

        }
    }

}

