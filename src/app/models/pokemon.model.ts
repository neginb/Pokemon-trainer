// the interfaces of the pokemon API

export interface Pokemon {
    name: string;
    url: string;
    id: number;
    
}

export interface PokemonResults {
    results: Array<Pokemon>;
    
}
