import { Pokemon } from "./pokemon.model";

// the interface of the user API
export interface User {
    
    id: number;
    username: string;
    pokemon: Pokemon[];
    
}