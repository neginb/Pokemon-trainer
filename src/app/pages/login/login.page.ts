import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage{

  constructor(private readonly router:Router) { }

  //take the user to the pokemons catalogue page after logging in process
  handleLogin(): void {
    this.router.navigateByUrl("/pokemons");
    
  }

}
