import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})

export class ProfilePage implements OnInit {

  //getting user from service
  get user(): User | undefined {
    return this.userService.user;

  }
  username?: string = "";

  //getting the selected pokemons by the user from userService
  get pokemons(): Pokemon[] {
    
    if(this.userService.user) {
      return this.userService.user.pokemon;
    }
    return [];
  }

  constructor(
    private userService: UserService,
  ) { }

  //getting the username of the user
  ngOnInit(): void {
    this.username= this.user?.username;
  }

}
