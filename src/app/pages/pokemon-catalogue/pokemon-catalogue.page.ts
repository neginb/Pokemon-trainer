import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})

export class PokemonCataloguePage implements OnInit {

  //take the pokemons from service
  get pokemons(): Pokemon[] {
    return this.pokemonCatalogueService.pokemons;
  }


  get loading(): boolean {
    return this.pokemonCatalogueService.loading;
  }

  //error handle
  get error(): string {
    return this.pokemonCatalogueService.error;
  }


  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }

  //communicating with the service and find all the pokemons from API
  ngOnInit(): void {
    this.pokemonCatalogueService.findAllPokemones();
  }

}
