# PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.6 to build a Pokémon Trainer web app.

## Install
```
git clone "https://gitlab.com/neginb/Pokemon-trainer"
cd Pokemon-trainer
npm install -g @angular/cli
```

## Usage

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/` to open the web app in your browser. 
The application will automatically reload if you change any of the source files.

You need to create an "environments" map in the "src/app", the root of the project, containing two files. 
Name the first file to "environment.prod.ts" and the other file to "environment.ts".

Here you create environment.ts file:
```
export const environment = {
production: false,
apiUsers:<your url>,
apiPokemons:<your url>,
API_KEY:<your key> 
};
```
Here you create environment.prod.ts file:
```
export const environment = {
production: true,
apiUsers:<your url>,
apiPokemons:<your url>,
API_KEY:<your key> 
};
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## contributors
 Negin Bakhtiarirad (@neginb) and Betiel Yohannes (@betielyohannes)

 ## Contributing
 No contributions allowed.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
